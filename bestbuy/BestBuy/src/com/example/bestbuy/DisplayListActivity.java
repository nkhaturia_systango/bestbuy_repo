package com.example.bestbuy;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.Toast;

public class DisplayListActivity extends ListActivity {
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
	 }
	public void display_list(String[] Department_name){
		MyArrayAdapter adapter = new MyArrayAdapter(this, Department_name);
		setListAdapter(adapter);
	 }
	@Override
    public void onBackPressed() {
     Toast.makeText(this, "You selected: " + "keyword", Toast.LENGTH_LONG).show();
		super.onBackPressed();
    }
}
